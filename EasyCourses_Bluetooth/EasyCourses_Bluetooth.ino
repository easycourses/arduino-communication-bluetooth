/*
  Présentation du programme
    Programme de communication Bluetooth
    par l'équipe EasyCourses - Pierre Lagourgue - 2015
    créé par Thomas KOWALSKI
  Notes du constructeur
    [Aucune]
  Notes des programmeurs :
    - Eviter d'utiliser la carte Bluetooth et d'envoyer un programme à l'Arduino en même temps.
    - Le mot de passe peut être modifié à la ligne commençant par "char* cartPassword [...]"
  Idées d'améliorations :
    [Aucune]
*/

#include <OneWire.h> // Inclusion de la librairie OneWire
#define DS18B20 0x28 // Adresse 1-Wire du DS18B20
#define BROCHE_ONEWIRE 7 // Broche utilisée pour le bus 1-Wire
#include <SPI.h>

/* Capteur de température */
float temp = 0;
OneWire ds(BROCHE_ONEWIRE);
boolean getTemperature(float *temp) {
  byte data[9], addr[8];

  if (!ds.search(addr)) { // Recherche un module 1-Wire
    ds.reset_search(); // Réinitialise la recherche de module
    return false; // Retourne une erreur
  }
  if (OneWire::crc8(addr, 7) != addr[7]) // Vérifie que l’adresse a été correctement reçue
    return false; // Si le message est corrompu on retourne une erreur

  if (addr[0] != DS18B20) // Vérifie qu’il s’agit bien d’un DS18B20
    return false; // Si ce n’est pas le cas on retourne une erreur

  ds.reset(); // On reset le bus 1-Wire
  ds.select(addr); // On sélectionne le DS18B20

  ds.write(0x44, 1); // On lance une prise de mesure de température
  delay(4); // Et on attend la fin de la mesure

  ds.reset(); // On reset le bus 1-Wire
  ds.select(addr); // On sélectionne le DS18B20
  ds.write(0xBE); // On envoie une demande de lecture du scratchpad

  for (byte i = 0; i < 9; i++) // On lit le scratchpad
    data[i] = ds.read(); // Et on stock les octets reçus

  // Calcul de la température en degré Celsius
  *temp = ((data[1] << 8) | data[0]) * 0.0625;

  // Pas d’erreur
  return true;
}

// Variables des capteurs
double temperature = 0;
double dureeFonctionnement = 0;
double niveauBatterie = 0;

// Variables "utilisateur"
char* cartPassword = "lagourgue"; // Mot de passe pour accéder au chariot. Il ne peut pas être changé.
const unsigned long periodicMessageFrequency = 1000; // Fréquence d'envoi des messages (en millisecondes)

// Variables utilisées dans les fonctions principales
boolean waitingPassword = false; //Variable définissant si on attend l'envoi du mot de passe sur le port série.
boolean connected = false;       // Variable définissant si oui ou non on est connecté à un téléphone.
boolean avertissementSonore = false; //Est ce qu'on est en train d'avertir l'utilisateur
boolean avertissementSonoreStop = false; //Est ce qu'on a dit au système d'arrêter de crier

// Variables système
char* ret; // Utilisée pour la conversion en tableaux de char, déclarée en public pour pouvoir la supprimer après son utilisation et éviter le memory overflow.
unsigned long time = 0; //Temps de fonctionnement.
int millisLED; //Moment où on a allumé les LED pour la dernière fois
int millisTemp; //Moment où on a relevé la température pour la dernière fois

//Méthode d'initialisation
void setup() {
  Serial.begin(9600); //Démarrage de la liaison avec le module Bluetooth
  //Broches pour la communication avec l'autre Arduino
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  //Broche pour le piezo-buzzer
  pinMode(5, OUTPUT);
  //Broches pour les LED (Batterie)
  pinMode(A0, INPUT); //Entrée du niveau de batterie
  pinMode(A5, INPUT); //Entrée du bouton d'affichage de la batterie
  pinMode(8, OUTPUT); //Sorties des LED
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
}

void loop() {
  if (millis() - millisTemp > 3000 && !avertissementSonoreStop) { //Si jamais ça fait plus de trois secondes qu'on n'a pas mesuré la température et que le chariot n'a pas déjà sonné 
    millisTemp = millis(); //On dit qu'on a mesuré la température maintenant
    if (getTemperature(&temp) && temp >= -0.81) { //Si la température est au dessus de -0.81°C (pour laisser encore quarante minutes avant la décongélation)
      musique(); //On joue la musique ^_^
      avertissementSonore = true; //Et on dit qu'on est en train de la ajouer
    }
  }
  if (millis() - millisLED > 3000 && millisLED != 0) { //Extinction des LED d'affichage
    millisLED = 0; //Réinitialisation du temps d'affichage
    digitalWrite(8, LOW); //Extinction des LED
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
    digitalWrite(12, LOW);
    digitalWrite(13, LOW);
  }
  if (Serial.available()) { //Si on reçoit un message du module Bluetooth
    int commandSize = (int)Serial.read(); //On lit la taille du message
    char command[commandSize]; //On déclare la chaîne qui sera de la taille du message
    int commandPos = 0;
    while (commandPos < commandSize) { //Et on lit le caractère jusqu'à qu'on les ait tous lus
      if (Serial.available()) {
        command[commandPos] = (char)Serial.read();
        commandPos++;
      }
    }
    command[commandPos] = 0; 
    processCommand(command); //Et on analyse notre message
  }
  unsigned long currentTime = millis(); 
  if (((currentTime - time) > periodicMessageFrequency)) { //Si on a pas envoyé de message depuis plus de la période d'envoi des messages périodiques
    sendPeriodicMessages(); //On les envoie
    time = currentTime; //Et on dit qu'on vient de les envoyer
  }
  if (digitalRead(A5) && (millis() - millisLED > 5000 || millisLED == 0)) //Si le bouton est appuyé et qu'on n'est pas en train de faire sonner l'alarme
    afficherBatterie(); //On affiche le niveau de batterie
  if (digitalRead(A5) && avertissementSonore) //Si on est en train de faire sonner l'alarme et qu'on appuie sur le bouton
    avertissementSonoreStop = true; //On arrête l'alarme
}

//Appelée quand on reçoit un message [interpréation]
void processCommand(char* command) {
  if (strcmp(command, "CAN_I_CONNECT:REQUEST") == 0) { //Si c'est une demande de connexion
    sendMessage("CAN_I_CONNECT:GO");                   //Et on demande au téléphone d'envoyer le mot de passe
    waitingPassword = true;                            //Et on dit qu'on attend le mot de passe
  } else if (waitingPassword) {                        //Si on attend le mot de passe
    waitingPassword = false;                           //On ne l'attend plus
    if (strcmp(cartPassword, command) == 0) {          //Si le mot de passe est correct
      sendMessage("CAN_I_CONNECT:PWOK");               //On "dit" au téléphone que la connexion est validée
      connected = true;
    } else {                                           //Sinon
      sendMessage("CAN_I_CONNECT:PWERR");              //On lui "dit" que non
    }
  } else {
    /* Tableau des codes et de leurs correspondances :
      2 - 3 - 4 => COMMANDE
      0 / 0 / 0 => Déconnecter
      0 / 0 / 1 => Arrêt
      1 / 0 / 0 => Droite
      1 / 0 / 1 => Gauche
      1 / 1 / 0 => Avancer
      1 / 1 / 1 => Reculer
    */
    //Extinction de toutes les sorties.
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    //Sélection du signal à envoyer
    if (strcmp(command, "MOVEMENT:FORWARD") == 0) {
      digitalWrite(2, HIGH);
      digitalWrite(3, HIGH);
    } else if (strcmp(command, "MOVEMENT:RIGHT") == 0) {
      digitalWrite(2, HIGH);
    } else if (strcmp(command, "MOVEMENT:BACKWARD") == 0) {
      digitalWrite(2, HIGH);
      digitalWrite(3, HIGH);
      digitalWrite(4, HIGH);
    } else if (strcmp(command, "MOVEMENT:LEFT") == 0) {
      digitalWrite(2, HIGH);
      digitalWrite(4, HIGH);
    } else if (strcmp(command, "MOVEMENT:STOP") == 0) {
      digitalWrite(4, HIGH);
    } else if (strcmp(command, "MOVEMENT:DISCONNECT") == 0) {
      //Ne rien faire car tout est déjà éteint !
    }
  }
}

//Envoyer un message au téléphone
void sendMessage(char* message) {
  int messageLen = strlen(message); //On mesure la taille du message
  if (messageLen < 256) {
    Serial.write(messageLen); //On écrit la taille du message sur le port série
    Serial.print(message); //Et on l'envoie
  }
}

// Méthode appelée toutes les x millisecondes pour envoyer les informations au téléphone.
void sendPeriodicMessages() {
  if (connected) {
    getTemperature(&temp); //On récupère la température
    temperature = (double)temp;
    sendMessage(createMessage("TEMPERATURE:", temperature)); //On envoie la température
    delay(15); //Permet d'attendre un petit temps que le message soit transmis
    sendMessage(createMessage("BATTERY:", analogRead(A0))); //On envoie la valeur de la batterie
    delay(15);
    dureeFonctionnement = (int)(millis() / 1000); //On envoie la durée de fonctionnement en secondes
    sendMessage(createMessage("TIME:", dureeFonctionnement));
    delete[] ret; //Et on libère la mémoire de ret
  }
}

//Méthode appelée pour envoyer les informations périodiques. Renvoie un tableau de char à partir d'une chaîne (l'identifiant d'information) et d'un nombre (la valeur).
char* createMessage(String infoName, double value) { //Fonction de qui créé le message à envoyer (en tableau de char)
  infoName += String(value);
  return strToChar(infoName);
}
char* strToChar(String s) { //Fonction qui convertir un String en char*
  unsigned int bufSize = s.length() + 1;
  ret = new char[bufSize];
  s.toCharArray(ret, bufSize);
  return ret;
}

void afficherBatterie() { //Mesure du niveau de batterie
  int battery = analogRead(A0); //On récupère le niveau du pont diviseur. La batterie va de 10.2 V à 14.5V (en théorie...)
  digitalWrite(8, HIGH); //On éteint jamais toutes les LED, une reste toujours allumée pour montrer que c'est encore allumé
  if (battery > 360) { //Et tous les 0.3V on allume une LED de plus.
    digitalWrite(9, HIGH);
    delay(50); //On attend un peu pour faire un effet "MacBook"
  }
  if (battery > 390) {
    digitalWrite(10, HIGH);
    delay(50);
  }
  if (battery > 420) {
    digitalWrite(11, HIGH);
    delay(50);
  }
  if (battery > 450) {
    digitalWrite(12, HIGH);
    delay(50);
  }
  if (battery > 480) {
    digitalWrite(13, HIGH);
    delay(50);
  }
  millisLED = millis(); //Attente pour la lecture puis effacement
}
void musique() {
  int tones[] = {392, 784, 660, 392, 660, 522}; //Musique de la sonnerie du lycée (G G E G E C)
  for (int i = 0; i < 6; i++) { //On joue chaque note
    if(digitalRead(A5)) { //Si jamais on a appuyé sur le bouton d'arrêt
      noTone(5); //On éteint la musique et on sort de la fonction
      return;
    }
    tone(5, tones[i]); //On joue la note i
    if (i == 2) //Et si jamais c'est la troisième note, on attend un peu plus 
      delay(1000);
    else
      delay(500);
  }
  noTone(5); //On arrête la note
}
